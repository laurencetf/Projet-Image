/*!------------------------------------------------------------ 
  \version	1.0
  \date		 Wed Dec 20 15:13:28 2017
  \author	 Fabien LAURENCET <laurencetf@eisti.eu>
  \brief 	Picture treatment program ( source code ) 
  \file		pictureTreatment.c
  -------------------------------------------------------------*/


#include "pictureTreatment.h"


/*!-------------------------------------------------------
  \fn      picturePBM openPBMFile (char str_fileName[])
  \author	  Fabien LAURENCET <laurencetf@eisti.eu>
  \date     Thu Jan  4 20:51:57 2018
  \param	 char str_fileName[] : the name of the picture to open
  \brief    Open a picture which have a pbm format
  \return	  a picturePBM structure which represent the picture
  ----------------------------------------------------------*/
picturePBM openPBMFile(char fileName[]) {
        FILE* file;
        picturePBM pic;
        int i;		
        int j;
        int int_value;
        i = 0;
        j = 0;
        file = fopen(fileName, "r");
        if (file == NULL) {
                printf("File not found maybe you did a mistake during the input of the name\n");
                pic.width = 0;
                pic.height = 0;
        } else {
                //Get the type of the file 
                fgets(pic.type, 50, file); 					
                //Get the comment at the second line of the file
                fgets(pic.comment, 250, file);				
                //Get picture dimensions, the height and the width of the picture
                fscanf(file, "%d %d\n", &pic.width, &pic.height);
                //Result table of table of pixel memory allocation
                pic.table = malloc((pic.width) * sizeof(int*)); 
                while (i < pic.width) {
                        //Result table of pixel memory allocation
                        pic.table[i] = malloc((pic.height) * sizeof(int));
                        while (j < pic.height) {
                                fscanf(file,"%d\n", &int_value);
                                //Get colors values of a picture 
                                pic.table[i][j] = int_value;
                                j++;
                        }
                        j = 0;
                        i++;
                }
        }
        //Close the file and return the picturePBM typed data
        fclose(file);
        return(pic);
}


/*!-------------------------------------------------------
  \fn      picturePGM openPGMFile (char str_fileName[])
  \author	  Fabien LAURENCET <laurencetf@eisti.eu>
  \date     Thu Jan  4 20:51:57 2018
  \param	 char str_fileName[] : the name of the picture to open
  \brief    Open a picture which have a pgm format
  \return	  a picturePGM structure which represent the picture
  ----------------------------------------------------------*/
picturePGM openPGMFile(char fileName[]) {
        FILE* file;
        picturePGM pic;
        int i;		
        int j;
        char tmp_colorMax[8];
        int int_value;
        i = 0;
        j = 0;
        file = fopen(fileName, "r");
        if (file == NULL) {
                printf("File not found maybe you did a mistake during the input of the name\n");
                pic.width = 0;
                pic.height = 0;
        } else {
                //Get the type of the file 
                fgets(pic.type, 50, file); 					
                //Get the comment at the second line of the file
                fgets(pic.comment, 250, file);				
                //Get picture dimensions, the height and the width of the picture
                fscanf(file, "%d %d\n", &pic.width, &pic.height);
                //Get the maximum value of a color of the picture
                fgets(tmp_colorMax, 8, file);
                pic.colorMax = atoi(tmp_colorMax);
                //Result table of table of pixel memory allocation
                pic.table = malloc((pic.width) * sizeof(int*)); 
                while (i < pic.width) {
                        //Result table of pixel memory allocation
                        pic.table[i] = malloc((pic.height) * sizeof(int));
                        while (j < pic.height) {
                                fscanf(file,"%d\n", &int_value);
                                //Get colors values of a picture 
                                pic.table[i][j] = int_value;
                                j++;
                        }
                        j = 0;
                        i++;
                }
        }
        //Close the file and return the picture typed data
        fclose(file);
        return(pic);
}


/*!-------------------------------------------------------
  \fn      picturePPM openPPMFile (char str_fileName[])
  \author	  Fabien LAURENCET <laurencetf@eisti.eu>
  \date     Thu Jan  4 20:51:57 2018
  \param	 char str_fileName[] : the name of the picture to open
  \brief    Open a picture which have a ppm format
  \return	  a picturePPM structure which represent the picture
  ----------------------------------------------------------*/
picturePPM openPPMFile(char fileName[]) {
        FILE* file;
        picturePPM pic;
        int i;		
        int j;
        char tmp_colorMax[8];
        int color[3];
        file = fopen(fileName, "r");
        i = 0;
        j = 0;
        if (file == NULL) {
                printf("File not found maybe you did a mistake during the input of the name\n");
                pic.width = 0;
                pic.height = 0;
        } else {
                //Get the type of the file 
                fgets(pic.type, 50, file); 					
                //Get the comment at the second line of the file
                fgets(pic.comment, 250, file);				
                //Get picture dimensions, the height and the width of the picture
                fscanf(file, "%d %d\n", &pic.width, &pic.height);
                //Get the maximum value of a color of the picture
                fgets(tmp_colorMax, 20, file);
                pic.colorMax = atoi(tmp_colorMax);
                //Result table of table of pixel memory allocation
                pic.table = malloc((pic.width) * sizeof(pixel*)); 
                while (i < pic.width) {
                        //Result table of pixel memory allocation
                        pic.table[i] = malloc((pic.height) * sizeof(pixel));
                        while (j < pic.height) {
                                fscanf(file,"%d\n%d\n%d\n", &color[0], &color[1], &color[2]);
                                //Get colors values of a picture 
                                pic.table[i][j].red = color[0];
                                pic.table[i][j].green = color[1];
                                pic.table[i][j].blue = color[2];
                                j++;
                        }
                        j = 0;
                        i++; 
                }
        }
        //Close the file and return the picture typed data
        fclose(file);
        return(pic);
}


/*!-------------------------------------------------------
  \fn      picturePGM colorToGrey( picturePPM pic)
  \author	  Fabien LAURENCET <laurencetf@eisti.eu>
  \date     Thu Jan  4 20:51:57 2018
  \param	 picturePPM pic : a colored picture ppm formatted thatchar str_fileName[] : the name of the picture to open we want to set in grey
  \brief    Set a colored picture in grey
  \return	  a picturePGM structure which represent the picture in grey
  ----------------------------------------------------------*/
picturePGM colorToGrey( picturePPM pic) {
        //Variables declarations
        picturePGM pic_result;
        int i;
        int j;
        //Variables initializations
        pic_result.type[0] = 'P';
        pic_result.type[1] = '2';
        pic_result.width = pic.width;
        pic_result.height = pic.height;
        pic_result.colorMax = pic.colorMax;
        //Memory allocation of the result variable (table of table of int)
        pic_result.table = malloc(pic.width * sizeof(int *));
        //Run the table in order to set every pixel in grey value
        for (i = 0 ; i < pic.width ; ++i) {
                //Memory allocation of the result variable (table of int)
                pic_result.table[i] = malloc(pic.height * sizeof(int));														
                for (j = 0 ; j < pic.height ; ++j) {
                        //Calculate the grey value of a pixel 
                        pic_result.table[i][j] = (int) (0.299 * pic.table[i][j].red + 0.587 * pic.table[i][j].green + 0.114 * pic.table[i][j].blue);														
                }
        }
        //Return the grey picturePGM picture;
        return pic_result;
}

/*!-------------------------------------------------------
  \fn      picturePBM binarize( int int_treshold, picturePGM pic)
  \author	  Fabien LAURENCET <laurencetf@eisti.eu>
  \date     Thu Jan  4 20:51:57 2018
  \param int int_treshold : the treshold value that enable to binarize the grey picture
  \param	 picturePGM pic : a grey picture pgm formatted that we want to set in black&white
  \brief    Set a grey picture in black&white
  \return	  a picturePBM structure which represent the black&white picture
  ----------------------------------------------------------*/
picturePBM binarize(int int_treshold, picturePGM pic) {
        //Variables declarations
        int x;
        int y;
        picturePBM pic_result;
        //Variables initializations
        pic_result.type[0] = 'P';
        pic_result.type[1] = '1';
        pic_result.width = pic.width;
        pic_result.height = pic.height;
        //Memory allocation of the result variable (table of table of int)
        pic_result.table = malloc(pic.width * sizeof(int *));
        //Run the table in order to binarize every pixel with the treshold
        for (x = 0 ; x < pic.width ; ++x) {
                //Memory allocation of the result variable (table of int)
                pic_result.table[x] = malloc(pic.height * sizeof(int));														
                for (y=0 ; y < pic.height ; ++y) {
                        //Compare the value of a pixel with the treshold value in order to know the pixel value binarized
                        if (pic.table[x][y] > int_treshold) {
                                //If higher than treshold then white
                                pic_result.table[x][y] = 1;													
                        } else{
                                //If lower than treshold black
                                pic_result.table[x][y] = 0;									
                        }
                }
        }
        //Return the binarized structure of picture
        return pic_result;
}


/*!-------------------------------------------------------
  \fn      int savePBMPic (picturePBM pic, char str_namr[])
  \author	  Fabien LAURENCET <laurencetf@eisti.eu>
  \date     Thu Jan  4 21:38:00 2018
  \param	 picturePBM pic : the picturePBM formatted picture we want to save
  \param	 char str_name[] : the name of the picture saved 
  \brief   Save a picture structure as a picture file
  \return	 Save pbm formatted picture
  ----------------------------------------------------------*/
int savePBMPic(picturePBM pic, char str_name[]) {
        //Variables declarations
        int x;										
        int y;			
        int int_res;
        FILE* file =NULL;
        //Opening the file in which we want to write the picture
        file = fopen(str_name,"w+");
        //Check if the file was correctly opened
        if (file == NULL) {
                printf("Unable to write in this file, check your rights\n");
                int_res = 0;									
        }else {
                fputs("P1\n",file);				
                fputs("#Picture treatment TP laurencetf©\n",file);
                fprintf(file,"%d %d\n", pic.width, pic.height);
                for (x = 0 ; x < pic.width ; x++) {									
                        for (y = 0 ; y < pic.height ; y++) {									
                                fprintf(file,"%d\n", pic.table[x][y]);     						
                        }	
                }
                int_res =1;			
        }
        //Close the file and return the result integer proofing that the program was well executed
        fclose(file);
        return int_res;										
}


/*!-------------------------------------------------------
  \fn      int savePGMPic (picturePGM pic, char str_namr[])
  \author	  Fabien LAURENCET <laurencetf@eisti.eu>
  \date     Thu Jan  4 21:38:00 2018
  \param	 picturePGM pic : the picturePGM formatted picture we want to save
  \param	 char str_name[] : the name of the picture saved 
  \brief   Save a picture structure as a picture file
  \return	 Save pgm formatted picture
  ----------------------------------------------------------*/
int savePGMPic(picturePGM pic, char str_name[]) {
        //Variables declarations
        int x;										
        int y;			
        int int_res;
        FILE* file =NULL;
        //Opening the file in which we want to write the picture
        file = fopen(str_name,"w+");
        //Check if the file was correctly opened
        if (file == NULL) {
                printf("Unable to write in this file, check your rights\n");
                int_res = 0;									
        }else {
                fputs("P2\n",file);				
                fputs("#Picture treatment TP laurencetf©\n",file);
                fprintf(file,"%d %d\n", pic.width, pic.height);
                fprintf(file, "%d\n", pic.colorMax);
                for (x = 0 ; x < pic.width ; x++) {									
                        for (y = 0 ; y < pic.height ; y++) {									
                                fprintf(file,"%d\n", pic.table[x][y]);     						
                        }	
                }
                int_res =1;			
        }
        //Close the file and return the result integer proofing that the program was well executed
        fclose(file);
        return int_res;										
}


/*!-------------------------------------------------------
  \fn      int savePPMPic (picturePPM pic, char str_namr[])
  \author	  Fabien LAURENCET <laurencetf@eisti.eu>
  \date     Thu Jan  4 21:38:00 2018
  \param	 picturePPM pic : the picturePPM formatted picture we want to save
  \param	 char str_name[] : the name of the picture saved 
  \brief   Save a picture structure as a picture file
  \return	 Save pPm formatted picture
  ----------------------------------------------------------*/
int savePPMPic(picturePPM pic, char str_name[]) {
        //Variables declarations
        int x;										
        int y;			
        int int_res;
        FILE* file =NULL;
        //Opening the file in which we want to write the picture
        file = fopen(str_name,"w+");
        //Check if the file was correctly opened
        if (file == NULL) {
                printf("Unable to write in this file, check your rights\n");
                int_res = 0;									
        }else {
                fputs("P3\n",file);				
                fputs("#Picture treatment TP©\n",file);
                fprintf(file,"%d %d\n", pic.width, pic.height);
                fprintf(file, "%d\n", pic.colorMax);
                for (x = 0 ; x < pic.width ; x++) {									
                        for (y = 0 ; y < pic.height ; y++) {
                                fprintf(file,"%d\n%d\n%d\n", pic.table[x][y].red, pic.table[x][y].green, pic.table[x][y].blue);     						
                        }	
                }
                int_res =1;			
        }
        //Close the file and return the result integer proofing that the program was well executed
        fclose(file);
        return int_res;										
}
