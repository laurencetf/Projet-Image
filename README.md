**README**

Compilation :
To compile the program and install the game please execute the command line below :
	"make"

Execution : 
To execute the program and play the game please execute the command line below :
	"./PictureTreatment"

	Parameters of the execution :
		-in fileInName : Name of the file we want to read, fileInName is the name of the file to read
		-out fileOutName : Name of the file we want to write in the picture after the modifications, fileOurName is the name of the file where we want to save the results
		-grey : transform a PPM colored picture in PBM grey picture 
		-treshold tresholdValue : binarize a picture with a treshold value, tresholdValue is an integer included between 0 and the colorMax value (4th line in a ppm or pgm file)
Doxyfile documentation generation :
To generate doxyfile documentation please execute the command line below :
	"make gendoc"


