#ce makefile implique une structure particulière :
#le tp doit etre un dossier
#dans chaque tp il doit y avoir un sous-dossier par exo
#le makefile se situe dans chaque sous-dossier d'exo, au même niveau que les dossiers src, bin, doc et save
ME = laurencetf#mon login
EDIT = emacs#mon éditeur pour faire la configuration du Doxyfile
PROG = $(bindir)PictureTreatment #nom du programme
EXO = Exercice#dénomination générique de l'exercice, suivi par le numéro de l'exercice
srcdir = ./#dossier des sources
bindir = ./#dossier des executables et des objets
docdir = ./#dossier contenant la doc
savedir = save/#dossier contenant les sauvegardes
tpdir = $(shell dir=$$(dirname `pwd`);echo $${dir\#\#*/};)#nom du dossier du tp
nomdir = $(ME)-$(shell tp=$(tpdir);echo $${tp%%_*} | tr '[:upper:]' '[:lower:]')#on formate correctement le nom du dossier de rendu
rendudir = ../$(nomdir)/#nom du dossier de rendu
CC = gcc -Wall #commande pour compiler
SRC = $(wildcard $(srcdir)*.c) #les sources
HEAD = $(wildcard $(srcdir)*.h) #extensions ;h
OBJ = $(subst $(srcdir), $(bindir), $(SRC:.c=.o)) #fichiers objets
CP = cp -uv $(srcdir)* $(savedir)#copie pour la sauvegarde
RES = cp -uv $(savedir)* $(srcdir)#restaure la dernière sauvegarde

.PHONY : clean save restore give gendoc

all : $(PROG)

$(PROG) : $(OBJ) #compilation du programme
	@$(CC) $^ -o $@ -lm

$(bindir)%.o : $(srcdir)%.c  #compilation des objets
	@$(CC) -c $^ -o $@

clean :
	@rm -f $(OBJ) #effacement des objets
	@find . -type f -name "*~"  -exec rm -if {} \;
	@find . -type f -name "#*#" -exec rm -if {} \;
	@find . -type f -name "*.bak" -exec rm -if {} \;

cleanrendu : #nettoyage uniquement pour le rendu
	@rm -Rf $(docdir)html
	@rm -Rf $(docdir)latex
	@rm -Rf $(bindir) *.o *.c~ *.h~

save : #sauvegarde dans le fichier save
	$(CP)

restore : #restore la derniere sauvegarde
	$(RES)

gendoc : #génère la documentation pour le rendu
	@$(EDIT) $(docdir)Doxyfile
	@cd $(docdir) && doxygen Doxyfile >/dev/null

#précondition au give : il ne faut pas qu'il y ait deja un dossier au dessus du même nom que celui qu'on va créer
give : #permet de mettre en place l'archive pour rendre les dossiers. Compile aussi afin de prévenir les erreurs de compilation et de documentation
	@cp -R ../$(tpdir) $(rendudir) #on copie le dossier dans un dossier temporaire pour éviter les modifications sur notre dossier principal (dans le cas ou il y ait un probleme)
	@echo "compilation"
	@find $(rendudir) -type d -name "$(EXO)*" -exec $(MAKE) -C {} \;
	@echo "génération de la documentation"
	@find $(rendudir) -type d -name "$(EXO)*" -exec $(MAKE) gendoc -C {} \;
	@echo "nettoyage"
	@find $(rendudir) -type d -name "$(EXO)*" -exec $(MAKE) clean -C {} \;
	@find $(rendudir) -type d -name "$(EXO)*" -exec $(MAKE) cleanrendu -C {} \;
	@echo "pour compiler le code d'un exercice tapez la commande \"make\"" >> $(rendudir)README
	@echo "pour executer le programme une fois compilé, tapez la commmande \"./$(PROG)\"" >> $(rendudir)README
	@echo "pour générer la documentation, tapez la commande \"cd $(docdir) && doxygen Doxyfile\"" >> $(rendudir)README
	@echo "la documentation se trouvera dans le dossier $(docdir)" >> $(rendudir)README
	@echo "compression"
	@tar -czf "../../$(nomdir).tgz" "../../$(nomdir)"
	@echo "suppression du dossier temporaire"
	@rm -Rf $(rendudir)
