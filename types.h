/*!------------------------------------------------------------ 
  \version	 1.0
  \date		 Thu Jan  4 21:52:06 2018
  \author	 Fabien LAURENCET <laurencetf@eisti.eu>
  \brief 	Picture treatment TP (types file) 
  \file		types.h
  -------------------------------------------------------------*/


#ifndef __TYPES_H_
#define __TYPES_h_


/*!---------------------------------------
 * \struct  pixel
 * \brief   a structure representing a pixel of three intger values representing RGB
 -----------------------------------------*/
//Define a structure as an equivalent type
typedef struct {    
        int red;
        int green;
        int blue;
} pixel;


/*!---------------------------------------
 * \struct picturePPM
 * \brief  A structure representing a ppm picture
 -----------------------------------------*/
//Define a structure as an equivalent type 
typedef struct{
        char type[2];
        char comment[70];
        int colorMax;
        int width;
        int height;
        pixel** table;
} picturePPM;


/*!---------------------------------------
 * \struct picturePGM
 * \brief  A structure representing a pgm picture
 -----------------------------------------*/
//Define a structure as an equivalent type 
typedef struct{
        char type[2];
        char comment[70];
        int colorMax;
        int width;
        int height;
        int** table;
} picturePGM;


/*!---------------------------------------
 * \struct picturePBM
 * \brief  A structure representing a pbm picture
 -----------------------------------------*/
//Define a structure as an equivalent type 
typedef struct{
        char type[2];
        char comment[70];
        int width;
        int height;
        int** table;
} picturePBM;


#endif
