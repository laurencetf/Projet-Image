/*!------------------------------------------------------------ 
  \version	1.0
  \date		 Thu Jan  4 21:54:51 2018
  \author	 Fabien LAURENCET <laurencetf@eisti.eu>
  \brief picture treatmen TP (header file) 
  \file		pictureTreatment.h
  -------------------------------------------------------------*/


#ifndef __PICTURETREATMENT_H_
#define __PICTURETREATMENT_H_


//Including standard library
#include <stdlib.h>
//Including string type library
#include <string.h>
//Including standard input/output library
#include <stdio.h>
#include "types.h"



/*!-------------------------------------------------------
  \fn      picturePBM openPBMFile (char str_fileName[])
  \author	  Fabien LAURENCET <laurencetf@eisti.eu>
  \date     Thu Jan  4 20:51:57 2018
  \param	 char str_fileName[] : the name of the picture to open
  \brief    Open a picture which have a pbm format
  \return	  a picturePBM structure which represent the picture
  ----------------------------------------------------------*/
picturePBM openPBMFile(char fileName[]);


/*!-------------------------------------------------------
  \fn      picturePGM openPGMFile (char str_fileName[])
  \author	  Fabien LAURENCET <laurencetf@eisti.eu>
  \date     Thu Jan  4 20:51:57 2018
  \param	 char str_fileName[] : the name of the picture to open
  \brief    Open a picture which have a pgm format
  \return	  a picturePGM structure which represent the picture
  ----------------------------------------------------------*/
picturePGM openPGMFile(char fileName[]);


/*!-------------------------------------------------------
  \fn      picturePPM openPPMFile (char str_fileName[])
  \author	  Fabien LAURENCET <laurencetf@eisti.eu>
  \date     Thu Jan  4 20:51:57 2018
  \param	 char str_fileName[] : the name of the picture to open
  \brief    Open a picture which have a ppm format
  \return	  a picturePPM structure which represent the picture
  ----------------------------------------------------------*/
picturePPM openPPMFile(char fileName[]);


/*!-------------------------------------------------------
  \fn      picturePGM colorToGrey( picturePPM pic)
  \author	  Fabien LAURENCET <laurencetf@eisti.eu>
  \date     Thu Jan  4 20:51:57 2018
  \param	 picturePPM pic : a colored picture ppm formatted thatchar str_fileName[] : the name of the picture to open we want to set in grey
  \brief    Set a colored picture in grey
  \return	  a picturePGM structure which represent the picture in grey
  ----------------------------------------------------------*/
picturePGM colorToGrey( picturePPM pic);


/*!-------------------------------------------------------
  \fn      picturePBM binarize( int int_treshold, picturePGM pic)
  \author	  Fabien LAURENCET <laurencetf@eisti.eu>
  \date     Thu Jan  4 20:51:57 2018
  \param int int_treshold : the treshold value that enable to binarize the grey picture
  \param	 picturePGM pic : a grey picture pgm formatted that we want to set in black&white
  \brief    Set a grey picture in black&white
  \return	  a picturePBM structure which represent the black&white picture
  ----------------------------------------------------------*/
picturePBM binarize(int int_treshold, picturePGM pic);


/*!-------------------------------------------------------
  \fn      int savePBMPic (picturePBM pic, char str_namr[])
  \author	  Fabien LAURENCET <laurencetf@eisti.eu>
  \date     Thu Jan  4 21:38:00 2018
  \param	 picturePBM pic : the picturePBM formatted picture we want to save
  \param	 char str_name[] : the name of the picture saved 
  \brief   Save a picture structure as a picture file
  \return	 Save pbm formatted picture
  ----------------------------------------------------------*/
int savePBMPic(picturePBM pic, char str_name[]);


/*!-------------------------------------------------------
  \fn      int savePGMPic (picturePGM pic, char str_namr[])
  \author	  Fabien LAURENCET <laurencetf@eisti.eu>
  \date     Thu Jan  4 21:38:00 2018
  \param	 picturePGM pic : the picturePGM formatted picture we want to save
  \param	 char str_name[] : the name of the picture saved 
  \brief   Save a picture structure as a picture file
  \return	 Save pgm formatted picture
  ----------------------------------------------------------*/
int savePGMPic(picturePGM pic, char str_name[]);


/*!-------------------------------------------------------
  \fn      int savePPMPic (picturePPM pic, char str_namr[])
  \author	  Fabien LAURENCET <laurencetf@eisti.eu>
  \date     Thu Jan  4 21:38:00 2018
  \param	 picturePPM pic : the picturePPM formatted picture we want to save
  \param	 char str_name[] : the name of the picture saved 
  \brief   Save a picture structure as a picture file
  \return	 Save pPm formatted picture
  ----------------------------------------------------------*/
int savePPMPic(picturePPM pic, char str_name[]);


#endif
