/*!------------------------------------------------------------ 
  \version	1.0
  \date		 Thu Jan  4 22:02:55 2018
  \author	 Fabien LAURENCET <laurencetf@eisti.eu>
  \brief 	Picture treatment TP (main prgram) 
  \file		main.c
  -------------------------------------------------------------*/


#include "pictureTreatment.h"


/*!-------------------------------------------------------
  \fn	  int main(int argc, char** argv)
  \author	  Fabien LAURENCET <laurencetf@eisti.eu>
  \date     Thu Jan  4 22:02:38 2018
  \param	 int argc The number of arguments placed in parameters during program execution
  \param	 char** argvThe values of arguments placed in parameters during program execution
  \brief     Main program that coordinates sub-program(functions and procedures) actions
  \return	  An integer which proof the program was well executed
  ----------------------------------------------------------*/
int main(int argc, char** argv) {
        int i;
        char tmp_in[40];
        char tmp_out[40];
        int int_grey;
        int int_bin;
        int int_treshold;
        FILE* file;
        char str_type[2];
        int_grey = 0;
        int_bin = 0;
        if(argc < 5) {
                printf("You didn't write enough parameters in the execution line refers to the readme to know the  parameters you must write\n");
        } else {
                for( i = 1;  i < argc ; ++i) {
                        if ( strcmp(argv[i], "-in") == 0 ) {
                                sprintf(tmp_in,"%s", argv[i+1]);
                        } else if ( strcmp(argv[i], "-out") == 0 ) {
                                sprintf(tmp_out,"%s", argv[i+1]);
                        } else if ( strcmp(argv[i], "-grey") == 0 ) {
                                int_grey = 1;
                        } else if ( strcmp(argv[i], "-treshold") == 0 ) {
                                int_grey = 1;
                                int_bin = 1;
                                int_treshold = atoi(argv[i+1]);
                        }
                }
                file = fopen(tmp_in, "r");
                if (file == NULL) {
                        printf("File not found maybe you did a mistake during the input of the name\n");
                } else {
                        fscanf(file,"%s\n", str_type);
                        fclose(file);
                        if (strcmp(str_type,"P3") == 0) {
                                picturePPM pic_in;
                                pic_in = openPPMFile(tmp_in);
                                if (int_bin == 1) {
                                        picturePBM pic_out;
                                        pic_out = binarize(int_treshold, colorToGrey(pic_in));
                                        savePBMPic(pic_out,tmp_out);
                                } else if (int_grey == 1) {
                                        picturePGM pic_out;
                                        pic_out = colorToGrey(pic_in);
                                        savePGMPic(pic_out,tmp_out);
                                } else {
                                        savePPMPic(pic_in, tmp_out);
                                }
                        } else if (strcmp(str_type,"P2") == 0) {
                                picturePGM pic_in;
                                pic_in = openPGMFile(tmp_in);
                                if (int_bin == 1) {
                                        picturePBM pic_out;
                                        pic_out = binarize(int_treshold, pic_in);
                                        savePBMPic(pic_out,tmp_out);
                                } else {
                                        savePGMPic(pic_in, tmp_out);
                                }
                        } else {
                                picturePBM pic_in;
                                pic_in = openPBMFile(tmp_in);
                                savePBMPic(pic_in, tmp_out);
                        }
                }
        }
}         
